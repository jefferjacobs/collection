import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import { environment } from 'environments/environment';

@Injectable()
export class SearchService {

  private nonce: string;

  constructor(
    private http: Http
  ) { }

  getNonce() {
    this.http.get('http://collection.jeffjacobs.net/api/get_nonce/?json=get_nonce&controller=posts&method=create_post')
    .subscribe(
      data => {
        this.nonce = data.json().nonce;
      }
    )
  }

  spotifySearch(search) {
    let url = 'https://api.spotify.com/v1/search?type=album&q=' + search;
    let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + environment.spotify_oauth });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(url, options);
  }

  createPost(album) {
    console.log('-----createPost');
    console.log(album);
    let data = {
      title: 'Test'
    }
    let url = 'http://collection.jeffjacobs.net/api/create_post/';
    let headers = new Headers({ 'X-WP-Nonce': this.nonce });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options);
  }

}
