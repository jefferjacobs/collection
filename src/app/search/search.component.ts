import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { SearchService } from 'app/search/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [SearchService]
})
export class SearchComponent implements OnInit {

 ///////////////////////////////////////
 // Properties
 ///////////////////////////////////////
 
 search = new FormControl();
 albums: any;

 ///////////////////////////////////////
 // Constructors
 ///////////////////////////////////////
 
 constructor(
   public searchService: SearchService,
 ) { }
 
 ///////////////////////////////////////
 // Functions
 ///////////////////////////////////////
 
 ngOnInit() {
   this.searchService.getNonce();
 }

 searchAlbum(event) {
   if (event.code == 'Enter') {
     this.searchService.spotifySearch(this.search.value)
      .subscribe(
        data => {
          this.albums = data.json().albums.items;
          console.log(data.json());
        },
        err => {
          console.error(err);
        }
      );
   }
 }

 createPost(album) {
  // this.searchService.createPost(album)
  //     .subscribe(
  //       data => {
  //         // this.albums = data.json().albums.items;
  //         console.log(data);
  //       },
  //       err => {
  //         console.error(err);
  //       }
  //     );
 }

}
