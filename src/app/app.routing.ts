

///////////////////////////////////////
// Core
///////////////////////////////////////
import { Routes, RouterModule } from '@angular/router';


///////////////////////////////////////
// Components
///////////////////////////////////////
import { SearchComponent } from 'app/search/search.component';


///////////////////////////////////////
// Routes
///////////////////////////////////////
const routes: Routes = [
  {
    path: '',
    redirectTo: 'albums',
    pathMatch: 'full'
  },
  {
    path: 'albums',
    loadChildren: 'app/albums/albums.module#AlbumsModule'
  },
  {
    path: 'search',
    component: SearchComponent
  }
];

export const AppRoutingModule = RouterModule.forRoot(routes);
