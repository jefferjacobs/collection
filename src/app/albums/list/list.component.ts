import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AlbumsService } from 'app/albums/albums.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  tag: any;
  albums: any;
  array: any;

  constructor(
    public route: ActivatedRoute,
    public albumsService: AlbumsService
  ) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.getAlbums(params['tag']);
    });

    this.albumsService.setlistShows();
    // this.albumsService.songkickShows();

  }

  getAlbums(tag) {
    // this.albums = null;
    this.albumsService.getAlbums(tag)
    .subscribe(
      data => {
        console.log(data.json());
        this.array = this.createArray(data.json());
        this.tag = data.json().tag;
        this.importAlbums(this.array);
      },
      err => {
        console.error(err);
      }
    );
  }

  createArray(data) {
    let array = [];
    for (let i = 0; i < data.posts.length; i++) {
      array.push(data.posts[i].custom_fields.album_id[0]);
    }
    return array;
  }

  importAlbums(array) {
    this.albumsService.spotifyAlbums(array)
     .subscribe(
        data => {
          this.albums = data.json().albums;
        },
        err => {
          console.error(err);
        }
      );
  }

}
