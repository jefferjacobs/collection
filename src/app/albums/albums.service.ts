import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import { environment } from 'environments/environment';

@Injectable()
export class AlbumsService {

  constructor(
    private http: Http
  ) { }

  getAlbums(tag) {
    if (tag == 'all') return this.http.get('http://collection.jeffjacobs.net/api/get_category_posts/?slug=albums&count=1000');
    else return this.http.get('http://collection.jeffjacobs.net/api/get_tag_posts/?slug=' + tag);
  }

  spotifyAlbums(array) {
    let url = 'https://api.spotify.com/v1/albums?ids=' + array;
    console.log(url);
    let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + environment.spotify_oauth });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(url, options);
  }

  setlistShows() {
    let url = 'https://api.setlist.fm/rest/1.0/setlist/3be08088';
    let headers = new Headers({ 'Accept': 'application/json', 'Authorization': 'x-api-key ' + environment.setlistfm_key });
    let options = new RequestOptions({ headers: headers });
    console.log(headers);
    this.http.get(url, options)
      .subscribe(
        data => {
          console.log('setlistShows');
          console.log(data);
        },
        err => {
          console.log('setlistShows');
          console.error(err);
        }
      )
  }

  songkickShows() {
    let url = 'http://api.songkick.com/api/3.0/users/jefferjacobs/gigography.json?apikey=';
    // let headers = new Headers({ 'Accept': 'application/xml', 'x-api-key': environment.setlistfm_key });
    // let options = new RequestOptions({ headers: headers });
    // console.log(options);
    this.http.get(url)
      .subscribe(
        data => {
          console.log('songkickShows');
          console.log(data);
        }
      )
  }

}
