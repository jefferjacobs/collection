import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlbumsRoutingModule } from './albums.routing';
import { AlbumsComponent } from './albums.component';
import { ListComponent } from './list/list.component';

@NgModule({
  imports: [
    CommonModule,
    AlbumsRoutingModule
  ],
  declarations: [AlbumsComponent, ListComponent]
})
export class AlbumsModule { }
