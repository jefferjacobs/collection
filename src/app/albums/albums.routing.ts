

///////////////////////////////////////
// Core/Vendor
///////////////////////////////////////
import { Routes, RouterModule } from '@angular/router';


///////////////////////////////////////
// Components
///////////////////////////////////////
import { AlbumsComponent } from 'app/albums/albums.component';
import { ListComponent } from 'app/albums/list/list.component';

///////////////////////////////////////
// Routing
///////////////////////////////////////
const routes: Routes = [
    {
    path: '',
    component: AlbumsComponent,
    children: [
      {
        path: '',
        redirectTo: 'all',
        pathMatch: 'full'
      },
      {
        path: ':tag',
        component: ListComponent
      }
    ]
  }
];

export const AlbumsRoutingModule = RouterModule.forChild(routes);
